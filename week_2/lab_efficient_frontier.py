# %% [markdown]
# # Lab: Efficient frontier
# ## Imports

# %%
import importlib

# %%
import matplotlib.pyplot as plt
import pandas as pd

from edhec_risk_kit import kit as erk

# %%
# In case I need to reload this module during development
importlib.reload(erk)

# %% [markdown]
# ## Part 1
# ### Read data

# %%
ind = erk.get_ind_returns()
ind.head()

# %% [markdown]
# ### Playing around with old functions
#
# This is not relevant to the topic of Efficient Frontier. Here the goal is
# to show that our functions work correctly with the new dataset.

# %%
_, ax = plt.subplots(layout="tight")
erk.drawdown(ind["Food"]).loc[:, "Drawdown"].plot.line(ax=ax)
plt.show()

# %%
erk.var_gaussian(
    ind[["Food", "Smoke", "Coal", "Beer", "Fin"]],
    modified=True,
)

# %% [markdown]
# ### Sharpe ratios

# %%
sharpe_ratios = erk.sharpe_ratio(ind, risk_free_rate=0.03, periods_per_year=12)
assert isinstance(sharpe_ratios, pd.Series)
sharpe_ratios = sharpe_ratios.sort_values()

# %%
_, ax = plt.subplots(layout="tight")
sharpe_ratios.plot.bar(
    title="Industry Sharpe Ratios 1926-2018",
    color="green",
    ax=ax,
)
plt.show()

# %%
# Shorter time period
sharpe_ratios = erk.sharpe_ratio(
    ind.loc["2000":, :],
    risk_free_rate=0.03,
    periods_per_year=12,
)
assert isinstance(sharpe_ratios, pd.Series)
sharpe_ratios = sharpe_ratios.sort_values()
_, ax = plt.subplots(layout="tight")
sharpe_ratios.plot.bar(
    title="Industry Sharpe Ratios 2000-2018",
    color="goldenrod",
    ax=ax,
)
plt.show()

# %% [markdown]
# ### Expected returns
#
# We'll pretend that the actual returns from 1995 to 2000 are the expected
# returns.

# %%
# Annualize monthly returns
er = erk.annualize_rets(ind.loc["1995":"2000", :], periods_per_year=12)
assert isinstance(er, pd.Series)
er = er.sort_values()

# %%
_, ax = plt.subplots(layout="tight")
er.plot.bar(ax=ax)
plt.show()

# %% [markdown]
# ### Covariance matrix

# %%
cov = ind.loc["1995":"2000", :].cov()
cov

# %% [markdown]
# To be continued...
