# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:percent
#     text_representation:
#       extension: .py
#       format_name: percent
#       format_version: '1.3'
#       jupytext_version: 1.14.4
#   kernelspec:
#     display_name: Python 3 (ipykernel)
#     language: python
#     name: python3
# ---

# %% [markdown]
# # Module 2 Graded Quiz
# ## Imports

# %%
from pathlib import Path

import numpy as np
import pandas as pd
import utils

# %% [markdown]
# ## Reading and preparing data (Questions 1-3)

# %%
data_path = Path(".").resolve().parent / "data"

file_name = "edhec-hedgefundindices.csv"
file_path = data_path / file_name

# %%
hfi_returns = pd.read_csv(file_path, index_col=0) / 100
hfi_returns.index = pd.to_datetime(hfi_returns.index, format="%d/%m/%Y").to_period("M")

hfi_returns.head()

# %% [markdown]
# ## Question 1
# Use the EDHEC Hedge Fund Indices data set that we used in the lab assignment
# as well as in the previous week's assignments. Load them into Python and
# perform the following analysis based on data since 2000 (including all of
# 2000): What was the Monthly Parametric Gaussian VaR at the 1% level (as a
# positive number) of the Distressed Securities strategy?

# %%
ds_returns = hfi_returns.loc["2000":, "Distressed Securities"]
level = 0.01

# %%
gaussian_var = utils.compute_gaussian_var(ds_returns, level)
gaussian_var

# %%
answer = gaussian_var * 100
print(f"Answer: {answer:.1f}")

# %% [markdown]
# ## Question 2
# Use the same data set at the previous question. What was the 1% VaR for the
# same strategy after applying the Cornish-Fisher Adjustment?

# %%
gaussian_var_cf = utils.compute_gaussian_var(ds_returns, level, adjusted=True)
gaussian_var_cf

# %%
answer = gaussian_var_cf * 100
print(f"Answer: {answer:.2f}")

# %% [markdown]
# ## Question 3
# Use the same dataset as the previous question. What was the Monthly Historic
# VaR at the 1% level (as a positive number) of the Distressed Securities
# strategy?

# %%
historic_var = utils.compute_historic_var(ds_returns, level)
historic_var

# %%
answer = historic_var * 100
print(f"Answer: {answer:.2f}")

# %% [markdown]
# ## Reading and preparing data (Questions 4-12)

# %%
file_name = "ind30_m_vw_rets.csv"
file_path = data_path / file_name

# %%
ind = pd.read_csv(file_path, index_col=0) / 100
ind.columns = ind.columns.str.strip()
ind.index = pd.to_datetime(ind.index, format="%Y%m").to_period("M")
ind.head()

# %%
# Selecting only the columns we'll actually use
ind = ind[["Books", "Steel", "Oil", "Mines"]]
ind.head()

# %% [markdown]
# ## Question 4
# Next, load the 30 industry return data using the `erk.get_ind_returns()`
# function that we developed during the lab sessions. For purposes of the
# remaining questions, use data during the 5 year period 2013-2017 (both
# inclusive) to estimate the expected returns as well as the covariance matrix.
# To be able to respond to the questions, you will need to build the MSR, EW and
# GMV portfolios consisting of the "Books", "Steel", "Oil", and "Mines"
# industries. Assume the risk free rate over the 5 year period is 10%.
#
# What is the weight of Steel in the EW Portfolio?

# %%
num_assets = ind.columns.size
answer = 100 / num_assets
print(f"Answer: {answer:.1f}")

# %% [markdown]
# ## Question 5
# What is the weight of the largest component of the MSR portfolio?

# %%
risk_free_rate = 0.1
expected_returns = utils.compute_annualized_returns(
    ind["2013":"2017"], periods_per_year=12
)
covariance_matrix = ind["2013":"2017"].cov()

# %%
weights_msr = utils.max_sharpe_ratio_portfolio(
    risk_free_rate, expected_returns, covariance_matrix
)
weights_msr

# %%
answer = weights_msr.max() * 100
print(f"Answer: {answer:.1f}")

# %% [markdown]
# ## Question 6
# Which of the 4 components has the largest weight in the MSR portfolio?

# %%
answer = weights_msr.idxmax()
print(f"Answer: {answer}")

# %% [markdown]
# ## Question 7
# How many of the components of the MSR portfolio have non-zero weights?

# %%
answer = weights_msr.apply(lambda x: not np.isclose(x, 0)).sum()
print(f"Answer: {answer}")

# %% [markdown]
# ## Question 8
# What is the weight of the largest component of the GMV portfolio?

# %%
weights_gmv = utils.global_minimum_volatility_portfolio(covariance_matrix)
weights_gmv

# %%
answer = weights_gmv.max() * 100
print(f"Answer: {answer:.2f}")

# %% [markdown]
# ## Question 9
# Which of the 4 components has the largest weight in the GMV portfolio?

# %%
answer = weights_gmv.idxmax()
print(f"Answer: {answer}")

# %% [markdown]
# ## Question 10
# How many of the components of the GMV portfolio have non-zero weights?

# %%
answer = weights_gmv.apply(lambda x: not np.isclose(x, 0)).sum()
print(f"Answer: {answer}")

# %% [markdown]
# ## Question 11
# Assume two different investors invested in the GMV and MSR portfolios at the
# start of 2018 using the weights we just computed. Compute the annualized
# volatility of these two portfolios over the next 12 months of 2018.
#
# What would be the annualized volatility over 2018 using the weights of the MSR
# portfolio?

# %%
cov_2018 = ind.loc["2018", :].cov()

# %%
volatility_msr = utils.compute_portfolio_volatility(weights_msr.to_numpy(), cov_2018)
annualized_volatility_msr = volatility_msr * np.sqrt(12)
annualized_volatility_msr

# %%
print(f"Answer: {annualized_volatility_msr * 100:.2f}")

# %% [markdown]
# ## Question 12
# What would be the annualized volatility over 2018 using the weights of the GMV
# portfolio?

# %%
volatility_gmv = utils.compute_portfolio_volatility(weights_gmv.to_numpy(), cov_2018)
annualized_volatility_gmv = volatility_gmv * np.sqrt(12)
annualized_volatility_gmv

# %%
print(f"Answer: {annualized_volatility_gmv * 100:.2f}")
