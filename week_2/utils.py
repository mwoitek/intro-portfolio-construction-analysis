from functools import partial

import numpy as np
import pandas as pd
from numpy.typing import NDArray
from scipy.optimize import minimize
from scipy.stats import norm

NumpyFloat = np.float_
FloatArray = NDArray[NumpyFloat]


def compute_annualized_return(returns: pd.Series, periods_per_year: int) -> NumpyFloat:
    num_observations = returns.size
    return np.prod(returns + 1) ** (periods_per_year / num_observations) - 1


def compute_annualized_returns(returns: pd.DataFrame, periods_per_year: int) -> pd.Series:
    func = partial(compute_annualized_return, periods_per_year=periods_per_year)
    return returns.apply(func)  # type: ignore


def compute_standardized_moment(data: pd.Series, degree: int) -> NumpyFloat:
    mu = data.mean()
    sigma = data.std(ddof=0)
    normalized_data = (data - mu) / sigma
    return np.mean(normalized_data**degree)


def compute_skewness(data: pd.Series) -> NumpyFloat:
    return compute_standardized_moment(data, degree=3)


def compute_kurtosis(data: pd.Series) -> NumpyFloat:
    return compute_standardized_moment(data, degree=4)


def compute_historic_var(returns: pd.Series, level: float = 0.05) -> NumpyFloat:
    return -np.quantile(returns, level)


def compute_cornish_fisher_adjustment(
    returns: pd.Series, z_score: NumpyFloat
) -> NumpyFloat:
    skewness = compute_skewness(returns)
    kurtosis = compute_kurtosis(returns)
    new_z_score = (
        z_score
        + (z_score**2 - 1) * skewness / 6
        + (z_score**3 - 3 * z_score) * (kurtosis - 3) / 24
        - (2 * z_score**3 - 5 * z_score) * skewness**2 / 36
    )
    return new_z_score


def compute_gaussian_var(
    returns: pd.Series, level: float = 0.05, adjusted: bool = False
) -> NumpyFloat:
    mu = returns.mean()
    sigma = returns.std(ddof=0)
    z_score: NumpyFloat = norm.ppf(level)  # type: ignore
    if adjusted:
        z_score = compute_cornish_fisher_adjustment(returns, z_score)
    return -(mu + sigma * z_score)


def compute_portfolio_return(
    weights: FloatArray, expected_returns: pd.Series
) -> NumpyFloat:
    return np.dot(weights, expected_returns)


def compute_portfolio_volatility(
    weights: FloatArray, covariance_matrix: pd.DataFrame
) -> NumpyFloat:
    prod_1 = np.matmul(covariance_matrix, weights)
    prod_2 = np.dot(weights, prod_1)
    return np.sqrt(prod_2)


def negative_sharpe_ratio(
    weights: FloatArray,
    risk_free_rate: float,
    expected_returns: pd.Series,
    covariance_matrix: pd.DataFrame,
) -> NumpyFloat:
    portfolio_return = compute_portfolio_return(weights, expected_returns)
    portfolio_volatility = compute_portfolio_volatility(weights, covariance_matrix)
    return (risk_free_rate - portfolio_return) / portfolio_volatility


def max_sharpe_ratio_portfolio(
    risk_free_rate: float, expected_returns: pd.Series, covariance_matrix: pd.DataFrame
) -> pd.Series:
    num_assets = expected_returns.size
    initial_guess = np.repeat(1 / num_assets, num_assets)
    bounds = ((0.0, 1.0),) * num_assets
    weights_sum_to_1 = {
        "type": "eq",
        "fun": lambda weights: np.sum(weights) - 1,
    }
    optimization_result = minimize(
        negative_sharpe_ratio,
        x0=initial_guess,
        args=(risk_free_rate, expected_returns, covariance_matrix),
        bounds=bounds,
        constraints=weights_sum_to_1,
        method="SLSQP",
        options={"disp": False},
    )
    return pd.Series(data=optimization_result.x, index=expected_returns.index)


def global_minimum_volatility_portfolio(covariance_matrix: pd.DataFrame) -> pd.Series:
    num_assets = covariance_matrix.shape[0]
    fake_expected_returns = pd.Series(
        data=np.repeat(1, num_assets),
        index=covariance_matrix.index,
    )
    return max_sharpe_ratio_portfolio(0.0, fake_expected_returns, covariance_matrix)
