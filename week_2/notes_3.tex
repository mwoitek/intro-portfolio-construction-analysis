% Created 2024-09-05 Thu 18:35
% Intended LaTeX compiler: pdflatex
\documentclass[11pt]{article}
\usepackage[american]{babel}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage{longtable}
\usepackage{wrapfig}
\usepackage{rotating}
\usepackage[normalem]{ulem}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{capt-of}
\usepackage{hyperref}
\usepackage{titling}
\usepackage[newfloat]{minted}
\usepackage{float}
\usepackage{enumitem}
\usepackage[a4paper,left=1cm,right=1cm,top=1cm,bottom=1cm]{geometry}
\setlength\parindent{0pt}
\setlength\droptitle{-50pt}
\posttitle{\par\end{center}\vspace{-5em}}
\renewcommand{\labelitemi}{$\rhd$}
\setlist[enumerate]{leftmargin=*}
\setlist[itemize]{leftmargin=*}
\setlist{nosep}
\pagenumbering{gobble}
\date{}
\title{Introduction to Optimization and the Efficient Frontier}
\hypersetup{
 pdfauthor={Marcio Woitek},
 pdftitle={Introduction to Optimization and the Efficient Frontier},
 pdfkeywords={},
 pdfsubject={},
 pdfcreator={Emacs 29.4 (Org mode 9.8)}, 
 pdflang={English}}
\begin{document}

\maketitle
\section*{The only free lunch in Finance}
\label{sec:org08527db}

\subsection*{2-asset combinations}
\label{sec:org48db996}

\begin{itemize}
\item Consider two assets with known return and volatility.
\item We plot these values on a 2D plane.
\item This plane is called \textbf{risk-return space} or \textbf{mean-variance framework}.
\item The \(x\)-axis corresponds to the risk, and the \(y\)-axis represents the return.
\item Question: What are the return and volatility of the combination of these
two assets?
\item Naive answer: The return/volatility of the combination is the weighted
average of the returns/volatilities.
\item For the return, this is indeed correct.
\item However, this isn't the case for the volatility.
\item The portfolio volatility depends on the \textbf{correlation} between the returns
for assets 1 and 2.
\end{itemize}
\subsection*{Mathematical formulation}
\label{sec:orgb9a53fc}

\begin{itemize}
\item 2-asset portfolio.
\item We have assets \(A\) and \(B\).
\item \(w_A\) is a fraction that represents how much is allocated to asset
\(A\). \(w_B\) is defined analogously.
\item The portfolio return is the weighted average of the returns for \(A\) and
\(B\):
\begin{equation}
\label{eq:portfolio-return-2}
R=w_A R_A+w_B R_B,
\end{equation}
where \(R_A\) and \(R_B\) denote the returns for \(A\) and \(B\),
respectively.
\item The formula for the portfolio volatility is more complicated.
\item We denote by \(\sigma_A\) and \(\sigma_B\) the volatilities for \(A\) and
\(B\).
\item Then the portfolio volatility \(\sigma\) satisfies the following
equation:
\begin{equation}
\label{eq:portfolio-volatility-2}
\sigma^2=w_A^2\sigma_A^2+w_B^2\sigma_B^2+2w_A w_B\sigma_A\sigma_B\rho_{A,B},
\end{equation}
where \(\rho_{A,B}\) denotes the correlation between \(R_A\) and \(R_B\).
\item This formula is a consequence of Eq. (\ref{eq:portfolio-return-2}). After
all, \(\sigma^2=\mathrm{Var}(R)\). And the last variance can be computed
with the aid of
\begin{equation}
\label{eq:variance-of-sum}
\mathrm{Var}(aX+bY)=a^2\:\mathrm{Var}(X)+b^2\:\mathrm{Var}(Y)+2ab\:\mathrm{Cov}(X,Y),
\end{equation}
where \(a\) and \(b\) are constants, \(X\) and \(Y\) are random
variables, and \(\mathrm{Cov}\) denotes the covariance. We also use the
fact that the covariance can be written in terms of the correlation
coefficient.
\end{itemize}
\subsection*{Consequences}
\label{sec:org06379a2}

\begin{itemize}
\item By carefully choosing \(w_A\) and \(w_B\), it's possible to obtain a
portfolio volatility \(\sigma\) that is less than \(\sigma_A\) and
\(\sigma_B\).
\item It's easier to obtain this kind of result when the returns are weakly
correlated.
\item This demonstrates the advantage of \textbf{diversifying}.
\end{itemize}
\section*{Markowitz Optimization and the Efficient Frontier}
\label{sec:orgf7ae891}

\subsection*{The efficient frontier}
\label{sec:org6aa7fa9}

\begin{itemize}
\item Same setup as last time.
\item Add a third asset \(C\).
\item Construct a portfolio using assets \(A\) and \(B\). Call this portfolio \(X\).
\item Construct a portfolio using assets \(B\) and \(C\). Call this portfolio \(Y\).
\item Now we can consider the combination of \(X\) and \(Y\), just as we did before.
\item In the risk-return space, assets \(A\), \(B\) and \(C\) define an entire
region of portfolios we can build.
\item It's not a good idea to hold portfolios that lie inside of this region!
\item For any such portfolio, you can always build
\begin{itemize}
\item another portfolio that has the same volatility but higher return; and
\item another portfolio that yields the same return but with less risk.
\end{itemize}
\item Roughly speaking, the best portfolios you can hold are located at the top
of the region's border.
\item This is the so-called \textbf{efficient frontier}.
\end{itemize}
\section*{Draw the efficient frontier}
\label{sec:org29459ce}

\subsection*{General formulae for the portfolio return and volatility}
\label{sec:orgd25798a}

\begin{itemize}
\item To draw the efficient frontier, we need expressions for the return and
volatility of a portfolio.
\item As we've seen, the portfolio return is just the weighted average of the
individual asset returns:
\begin{equation}
\label{eq:portfolio-return-general}
R_p=\sum_{i=1}^k w_i R_i.
\end{equation}
\item The general formula for the portfolio variance is
\begin{equation}
\label{eq:portfolio-variance-general}
\sigma_p^2=\sum_{i=1}^k\sum_{j=1}^k w_i w_j\sigma_i\sigma_j\rho_{ij}.
\end{equation}
\item These are natural generalizations of the formulae we had in the 2-asset case.
\end{itemize}
\subsection*{Matrix formulation}
\label{sec:org5a44ea6}

\begin{itemize}
\item We denote by \(\mathbf{w}\) the column vector containing all the weights \(w_i\).
\item We denote by \(\mathbf{R}\) the column vector containing all the asset
returns \(R_i\).
\item Then the portfolio return can be written as
\begin{equation}
\label{eq:portfolio-return-matrix}
R_p=\mathbf{w}^T\mathbf{R}.
\end{equation}
\item We denote by \(\boldsymbol{\Sigma}\) the covariance matrix associated
with the asset returns \(R_i\).
\item In matrix form, the formula for the portfolio variance is
\begin{equation}
\label{eq:portfolio-variance-matrix}
\sigma_p^2=\mathbf{w}^T\boldsymbol{\Sigma}\mathbf{w}.
\end{equation}
\end{itemize}
\subsection*{Plotting the efficient frontier}
\label{sec:org9b75ac5}

\begin{itemize}
\item We begin by considering two points in the risk-return space.
\item The first point corresponds to a portfolio built from only the asset with
the lowest return.
\item The second point is analogous. But we consider only the asset with the
highest return.
\item For every level of return between these points, we build a portfolio with
the least variance.
\item A portfolio on the efficient frontier has the minimum volatility for its
level of return.
\item \textbf{Mathematical formulation}: We want to find a set of weights
\(\mathbf{w}\) that minimize the quadratic form
\[\frac{1}{2}\mathbf{w}^T\boldsymbol{\Sigma}\mathbf{w}.\] Essentially,
this is the portfolio variance. The weights also have to satisfy the
constraints
\begin{align}
  \label{eq:weights-constraints}
  \begin{split}
    \mathbf{w}^T\mathbf{R}&=\mathbf{r}_0,\\
    \mathbf{w}^T\mathbf{1}&=\mathbf{1},\\
    \mathbf{w}&\geq0.
  \end{split}
\end{align}
The first constraint specifies the value of the portfolio return. The
other constraints guarantee that \(\mathbf{w}\) is a vector of proper
weights (they're all non-negative and add up to 1).
\end{itemize}
\end{document}
