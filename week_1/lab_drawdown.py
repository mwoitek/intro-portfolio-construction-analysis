# %% [markdown]
# # Lab: Drawdown
# ## Imports

# %%
import pandas as pd

# %% [markdown]
# ## Read data

# %%
rets = pd.read_csv(
    "../data/Portfolios_Formed_on_ME_monthly_EW.csv",
    header=0,
    index_col=0,
    na_values=-99.99,
)
rets.index = pd.to_datetime(rets.index, format="%Y%m").to_period("M")
rets = rets[["Lo 10", "Hi 10"]]
rets.columns = ["SmallCap", "LargeCap"]
rets = rets / 100

# %%
rets.plot.line()

# %% [markdown]
# ## Compute drawdowns
#
# 1. Compute a wealth index
# 2. Compute the previous peaks
# 3. Compute drawdown
#
# ### Compute a wealth index

# %%
wealth_index = 1000 * (1 + rets["LargeCap"]).cumprod()
wealth_index.head()

# %%
wealth_index.plot.line()

# %% [markdown]
# ### Compute the previous peaks

# %%
previous_peaks = wealth_index.cummax()

# %%
previous_peaks.plot.line()

# %% [markdown]
# ### Compute drawdown

# %%
# `drawdwn` is not a spelling mistake. Later a `drawdown` function will be
# defined. And I don't want to use the same name for two different things.
drawdwn = (wealth_index - previous_peaks) / previous_peaks
drawdwn.head()

# %%
drawdwn.plot()

# %% [markdown]
# Notice that the drawdown values are non-positive. Then, to compute the
# max drawdown, we actually find the minimum value!

# %%
# Max drawdown
drawdwn.min()

# %%
# Max drawdown since 1975
drawdwn["1975":].min()

# %%
# Period corresponding to the max drawdown
drawdwn["1975":].idxmin()

# %% [markdown]
# #### Function for computing drawdowns


# %%
def drawdown(return_series: pd.Series) -> pd.DataFrame:
    """
    Takes a time series of asset returns.
    Computes and returns a DataFrame that contains:
    - the wealth index,
    - the previous peaks,
    - percent drawdowns.
    """
    wealth_index = 1000 * (1 + return_series).cumprod()
    previous_peaks = wealth_index.cummax()
    drawdowns = (wealth_index - previous_peaks) / previous_peaks
    return pd.DataFrame(
        data={
            "Wealth": wealth_index,
            "Peaks": previous_peaks,
            "Drawdown": drawdowns,
        }
    )


# %%
# Testing this function
drawdown_df = drawdown(rets["LargeCap"])
drawdown_df.head()

# %%
drawdown_df["Drawdown"].min()

# %%
drawdown_df[["Wealth", "Peaks"]].plot()

# %%
drawdown(rets["SmallCap"]).loc[:, "Drawdown"].min()
