% Created 2024-08-28 Wed 09:07
% Intended LaTeX compiler: pdflatex
\documentclass[11pt]{article}
\usepackage[american]{babel}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage{longtable}
\usepackage{wrapfig}
\usepackage{rotating}
\usepackage[normalem]{ulem}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{capt-of}
\usepackage{hyperref}
\usepackage{titling}
\usepackage[newfloat]{minted}
\usepackage{float}
\usepackage{enumitem}
\usepackage[a4paper,left=1cm,right=1cm,top=1cm,bottom=1cm]{geometry}
\setlength\parindent{0pt}
\setlength\droptitle{-50pt}
\posttitle{\par\end{center}\vspace{-5em}}
\renewcommand{\labelitemi}{$\rhd$}
\setlist[enumerate]{leftmargin=*}
\setlist[itemize]{leftmargin=*}
\setlist{nosep}
\pagenumbering{gobble}
\date{}
\title{Notes: Fundamentals of risk and returns}
\hypersetup{
 pdfauthor={Marcio Woitek},
 pdftitle={Notes: Fundamentals of risk and returns},
 pdfkeywords={},
 pdfsubject={},
 pdfcreator={Emacs 29.4 (Org mode 9.8)}, 
 pdflang={English}}
\begin{document}

\maketitle
\section*{Fundamentals of returns}
\label{sec:orgab68637}

\begin{itemize}
\item Average return is misleading.
\item The performance of two assets with the same average return may be very different.
\item We need to take \textbf{volatility} into account.
\end{itemize}
\subsection*{From prices to returns}
\label{sec:orgcb82b1b}

\begin{itemize}
\item We denote the price of an asset at time \(t\) by \(P_t\).
\item The \textbf{return} obtained during the period from time \(t\) to time
\(t+1\) can be computed as follows:
\begin{equation}
\label{eq:return-1}
R_{t,t+1}=\frac{P_{t+1}-P_t}{P_t}.
\end{equation}
\item In the case of a stock that pays dividends, we need to add the
corresponding value to our "final" amount:
\begin{equation}
\label{eq:return-2}
R_{t,t+1}=\frac{P_{t+1}+D_{t,t+1}-P_t}{P_t},
\end{equation}
where \(D_{t,t+1}\) represents the dividend.
\item When we add this extra term, we're computing the \textbf{total return}.
\item If the dividend is \textbf{not} taken into account, we're computing the \textbf{price return}.
\item However, the use of the total return is more common.
\end{itemize}
\subsection*{Multi-period returns}
\label{sec:org9b73bea}

\begin{itemize}
\item How do we compute the return \(R_{t,t+2}\)?
\item For simplicity, we consider the price return.
\item The formula for \(R_{t,t+1}\) can be rewritten as follows:
\begin{align*}
R_{t,t+1}&=\frac{P_{t+1}-P_t}{P_t}\\
&=\frac{P_{t+1}}{P_t}-1\\
1+R_{t,t+1}&=\frac{P_{t+1}}{P_t}
\end{align*}
\item By replacing \(t\) with \(t+1\), we can also write
\begin{equation*}
1+R_{t+1,t+2}=\frac{P_{t+2}}{P_{t+1}}
\end{equation*}
\item We expect the desired return \(R_{t,t+2}\) to satisfy a similar relation:
\begin{equation*}
1+R_{t,t+2}=\frac{P_{t+2}}{P_t}
\end{equation*}
\item We can use this equation to derive an expression for \(R_{t,t+2}\):
\begin{align*}
1+R_{t,t+2}&=\frac{P_{t+2}}{P_t}\\
&=\frac{P_{t+1}}{P_t}\frac{P_{t+2}}{P_{t+1}}\\
&=\left(1+R_{t,t+1}\right)\left(1+R_{t+1,t+2}\right)\\
R_{t,t+2}&=\left(1+R_{t,t+1}\right)\left(1+R_{t+1,t+2}\right)-1
\end{align*}
\item This result shows that, to calculate a multi-period return, we have to
compound the single-period returns.
\end{itemize}
\subsection*{Annualizing returns}
\label{sec:org1f059cf}

\begin{itemize}
\item Often we need to compare returns over different periods of time.
\item For example, we may have to compare a monthly return to a quarterly return.
\item To do so, we compute \textbf{annualized returns}.
\item From the lecture: "The annualized return is nothing more than the return
you would get if the return that you're looking at had continued for a
year."
\end{itemize}
\subsubsection*{Examples}
\label{sec:orgbe59757}

\begin{itemize}
\item The return over the month is 1\%. What is the annualized return?
\begin{equation*}
(1+0.01)^{12}-1\approx0.1268=12.68\%
\end{equation*}
\item A stock gains 1\% over a quarter (i.e., a 3 month period). What is its
annualized return?
\begin{equation*}
(1+0.01)^4-1\approx0.0406=4.06\%
\end{equation*}
\end{itemize}
\section*{Measures of risk and reward}
\label{sec:org57705a7}

\subsection*{Volatility}
\label{sec:org6eac690}

\begin{itemize}
\item Volatility is a measure of how much the return deviates from its mean.
\item In other words, volatility is related to the variance/standard deviation
of the returns.
\item We denote by \(\bar{R}\) the arithmetic mean of the returns.
\item Then the corresponding variance can be expressed as
\begin{equation}
\label{eq:variance-1}
\sigma_R^2=\frac{1}{N}\sum_{i=1}^N\left(R_i-\bar{R}\right)^2,
\end{equation}
where \(R_i\) represents the return for the \(i\)-th period, and
\(N\) corresponds to the number of periods.
\item Our measure of volatility is the standard deviation \(\sigma_R\).
\end{itemize}
\subsection*{Annualizing volatility}
\label{sec:orge6f2f65}

\begin{itemize}
\item We cannot compare the volatility from daily data with the volatility from
monthly data.
\item To compare volatilities, we need to annualize them.
\item This can be done with the aid of this formula:
\begin{equation}
\label{eq:volatility-1}
\sigma_{\mathrm{ann}}=\sigma_p\sqrt{p},
\end{equation}
where \(\sigma_p\) denotes the volatility for any period that is not a
year, and \(p\) represents the number of such periods in a year.
\item For example, if we're dealing with a stock's daily return series, then
\begin{itemize}
\item \(\sigma_p\) is the volatility from daily data;
\item \(p=252\), since there are 252 trading days per calendar year;
\item the annualized volatility is given by
\(\sigma_{\mathrm{ann}}=\sigma_p\sqrt{252}\).
\end{itemize}
\end{itemize}
\subsection*{Risk-adjusted measures}
\label{sec:org65edaaa}

\begin{itemize}
\item When comparing two portfolios, we don't want to look at just the return.
\item An investment that yields a smaller return may be interesting if the
corresponding risk is low.
\item Then a more useful measure is the \textbf{return per unit of volatility}.
\item Return on risk ratio:
\begin{equation}
\label{eq:return-on-risk}
\text{Return on risk ratio}=\frac{\text{Return}}{\text{Volatility}}
\end{equation}
\item Instead of considering the return, we could use the \textbf{excess return}.
\item The excess return is the return minus the risk-free rate.
\item The risk-free rate is the rate of return of an investment with no risk
(e.g., a zero-coupon US Treasury bond).
\item Sharpe ratio:
\begin{equation}
\label{eq:sharpe-ratio}
\text{Sharpe ratio}=\frac{\text{Excess return}}{\text{Volatility}}
\end{equation}
\end{itemize}
\section*{Measuring max drawdown}
\label{sec:org3516b02}

\subsection*{Max drawdown}
\label{sec:org8875851}

\begin{itemize}
\item Volatility is not the only measure of risk.
\item Some people argue that volatility is not a good measure.
\item If the return deviates from its mean on the upside, then we are making money.
\item \textbf{Risk is the possibility of losing money}.
\item Imagine we buy a stock at its highest price, and sell it at its lowest price.
\item In this case, our loss corresponds to the maximum loss.
\item This is what we call the max drawdown, the worst possible return.
\end{itemize}
\subsection*{Constructing a wealth index}
\label{sec:orgb2f8438}

\begin{itemize}
\item Question: How do we compute the max drawdown?
\item Imagine we buy an asset and hold it.
\item Using the return series, we compute the growth of the initial investment
over time.
\item This is the so-called \textbf{wealth index}.
\item At any point in time, we identify the previous peak.
\item The drawdown corresponds to the difference between the prior peak and the
current wealth index.
\item Next, we use the time-series associated with the drawdowns to compute its
maximum value, which is precisely the max drawdown.
\end{itemize}
\subsection*{Risk adjustment ratio using drawdown}
\label{sec:orgf34dcde}

\begin{itemize}
\item When we use drawdowns, what is the equivalent of the return on risk ratio?
\item The answer is the \textbf{Calmar ratio}.
\item To determine this ratio, one usually considers \textbf{only the last 36 months}.
\item The Calmar ratio is then computed as the annualized return divided by the
max drawdown.
\end{itemize}
\subsection*{Some cautionary notes about drawdowns}
\label{sec:org129d32d}

\begin{itemize}
\item Drawdown is not a perfect measure of risk.
\item Its value depends on only two data points, which means this measure is
very sensitive to outliers.
\item Drawdown is highly dependent on the frequency of observations.
\item For example, a very deep drawdown based on daily data might almost
completely disappear when we analyze the corresponding monthly data.
\item Nonetheless, drawdown is a popular measure amongst practitioners.
\item However, there are more robust measures of extreme risk (e.g., VaR and CVaR).
\end{itemize}
\end{document}
