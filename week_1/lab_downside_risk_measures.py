# %% [markdown]
# # Lab: Semi-deviation, VaR and CVaR
# ## Imports

# %%
# import importlib

# %%
from typing import cast

import numpy as np
import pandas as pd
from scipy.stats import norm

from edhec_risk_kit import kit as erk

# %%
# In case I need to reload this module during development
# importlib.reload(erk)

# %% [markdown]
# ## Read hedge fund returns

# %%
hfi = erk.get_hfi_returns()
hfi.head()

# %% [markdown]
# ## Semi-deviation

# %%
hfi[hfi < 0].std(ddof=0)

# %%
erk.semideviation(hfi)

# %% [markdown]
# ## Value at Risk (VaR)
#
# 1. Historic VaR
# 2. Parametric VaR (Gaussian)
# 3. Modified Cornish-Fisher VaR
#
# ### Historic VaR

# %%
hfi.aggregate(lambda c: np.percentile(c, 5))

# %%
erk.var_historic(hfi)

# %% [markdown]
# ### Parametric VaR

# %%
z = norm.ppf(0.05)
-(hfi.mean() + z * hfi.std(ddof=0))

# %%
erk.var_gaussian(hfi)

# %% [markdown]
# ### Modified Cornish-Fisher VaR

# %%
erk.var_gaussian(hfi, modified=True)

# %% [markdown]
# ### Comparing different types of VaR

# %%
var_list = [
    erk.var_gaussian(hfi),
    erk.var_gaussian(hfi, modified=True),
    erk.var_historic(hfi),
]
var_list = cast(list[pd.Series], var_list)
comparison = pd.concat(var_list, axis=1)
comparison.columns = ["Gaussian", "Cornish-Fisher", "Historic"]

# %%
comparison.plot.bar(title="EDHEC Hedge Fund Indices: VaR")

# %% [markdown]
# ## Conditional Value at Risk (CVaR)

# %%
erk.cvar_historic(hfi)
