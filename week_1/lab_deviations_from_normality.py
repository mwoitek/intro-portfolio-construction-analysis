# %% [markdown]
# # Lab: Deviations from normality
# ## Imports

# %%
# import importlib

# %%
import numpy as np
import pandas as pd
import scipy.stats

from edhec_risk_kit import kit as erk

# %%
# In case I need to reload this module during development
# importlib.reload(erk)

# %% [markdown]
# ## Read hedge fund returns

# %%
hfi = erk.get_hfi_returns()
hfi.head()

# %% [markdown]
# ## Skewness

# %%
# Evidence of negative skewness: Mean is less than the median
pd.concat(
    [
        hfi.mean(),
        hfi.median(),
        hfi.mean() > hfi.median(),
    ],
    axis=1,
)

# %%
# Compute skewness using the function from the lecture
skew = erk.skewness(hfi)
assert isinstance(skew, pd.Series)
skew = skew.sort_values()
skew

# %%
# Compute skewness using the function from scipy
skew_alt = np.sort(scipy.stats.skew(hfi))
skew_alt

# %%
skew.to_numpy()

# %%
# Check if all values match
assert np.all(np.isclose(skew.to_numpy(), skew_alt))

# %% [markdown]
# ### Simulate normal returns

# %%
normal_rets = np.random.normal(0, 0.15, size=26300)
normal_rets = pd.Series(data=normal_rets)

# %%
# Should be close to zero
erk.skewness(normal_rets)

# %% [markdown]
# ## Kurtosis

# %%
# For normal returns, this should be close to 3
erk.kurtosis(normal_rets)

# %%
# Kurtosis for the hedge fund returns
erk.kurtosis(hfi)

# %%
# Using scipy: `kurtosis` returns the excess kurtosis!
# Should be close to zero:
scipy.stats.kurtosis(normal_rets)

# %%
# Check if values are consistent
assert np.isclose(
    erk.kurtosis(normal_rets) - 3,
    scipy.stats.kurtosis(normal_rets),
)

# %% [markdown]
# ## Jarque-Bera test

# %%
# Test result for Gaussian data
statistic, p_value = scipy.stats.jarque_bera(normal_rets)
p_value

# %%
erk.is_normal(normal_rets)

# %%
# Test results for the hedge fund returns
hfi.aggregate(erk.is_normal)

# %% [markdown]
# ## Testing our functions one more time

# %%
ffme = erk.get_ffme_returns()

# %%
erk.skewness(ffme)

# %%
erk.kurtosis(ffme)

# %%
ffme.aggregate(erk.is_normal)
