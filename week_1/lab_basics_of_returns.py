# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:percent
#     text_representation:
#       extension: .py
#       format_name: percent
#       format_version: '1.3'
#       jupytext_version: 1.14.4
#   kernelspec:
#     display_name: Python 3 (ipykernel)
#     language: python
#     name: python3
# ---

# %% [markdown]
# # Lab: Basics of Returns
# ## Formulae for return
# $$R_{t,t+1} = \frac{P_{t+1} - P_t}{P_t}$$
# or
# $$R_{t,t+1} = \frac{P_{t+1}}{P_t} - 1$$

# %%
import numpy as np
import pandas as pd

# %matplotlib inline

# %% [markdown]
# ## Computing returns with numpy

# %%
prices_arr = np.array([8.70, 8.91, 8.71])
prices_arr

# %%
prices_arr[1:] / prices_arr[:-1] - 1

# %% [markdown]
# ## Computing returns with pandas

# %%
prices_1 = pd.DataFrame(
    data={
        "BLUE": [8.70, 8.91, 8.71, 8.43, 8.73],
        "ORANGE": [10.66, 11.08, 10.71, 11.59, 12.11],
    }
)
prices_1

# %%
# Method 1
prices_1.iloc[1:].values / prices_1.iloc[:-1] - 1

# %%
# Method 2
prices_1 / prices_1.shift(1) - 1

# %%
# Method 3
prices_1.pct_change()

# %% [markdown]
# ## Example from the lecture

# %%
prices_2 = pd.read_csv("../data/sample_prices.csv")
prices_2

# %%
prices_2.plot()

# %%
returns = prices_2.pct_change()
returns

# %%
returns.plot.bar()

# %%
returns.mean()

# %%
returns.std()

# %%
# Compound return
(returns + 1).prod() - 1

# %% [markdown]
# ## Annualization

# %%
monthly_return = 0.01
annualized_return = (1 + monthly_return) ** 12 - 1
annualized_return
