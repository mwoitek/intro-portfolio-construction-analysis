# %% [markdown]
# # Lab: Risk-adjusted returns
# ## Imports

# %%
import numpy as np
import pandas as pd

# %% [markdown]
# ## Working with fake data
# ### Read data

# %%
prices = pd.read_csv("../data/sample_prices.csv")

# %% [markdown]
# ### Compute the returns

# %%
returns = prices.pct_change()
returns

# %%
returns = returns.dropna()
returns

# %% [markdown]
# ### Compute the volatility

# %%
# Simple method
returns.std()

# %%
# From scratch
deviations = returns - returns.mean()
squared_deviations = deviations**2
variance = squared_deviations.mean()
volatility = np.sqrt(variance)
volatility

# %% [markdown]
# These methods yield different results. This happens, since the `std`
# method uses the correct formula for the sample standard deviation. By
# that, I mean the equation with $N-1$ in the denominator.

# %%
# Correct volatility calculation from scratch
number_of_obs = returns.shape[0]
variance = squared_deviations.sum() / (number_of_obs - 1)
volatility = np.sqrt(variance)
volatility

# %% [markdown]
# ### Annualizing the volatility

# %%
# We're working with monthly data
returns.std() * np.sqrt(12)

# %% [markdown]
# ## Working with real portfolio data
# ### Read data

# %%
returns = pd.read_csv(
    "../data/Portfolios_Formed_on_ME_monthly_EW.csv",
    header=0,
    index_col=0,
    na_values=-99.99,
)
returns.index = pd.to_datetime(returns.index, format="%Y%m")
returns.head()

# %% [markdown]
# ### Fixing the DataFrame

# %%
# Select the columns we'll use
columns = ["Lo 10", "Hi 10"]
returns = returns[columns]
returns.head()

# %%
# Convert percentages to raw numbers
returns = returns / 100
returns.head()

# %%
# Rename columns
returns.columns = ["SmallCap", "LargeCap"]
returns.head()

# %%
returns.plot.line()

# %% [markdown]
# ### Compute the annualized volatility

# %%
annualized_vol = returns.std() * np.sqrt(12)
annualized_vol

# %% [markdown]
# ### Compute the annualized return

# %%
n_months = returns.shape[0]
return_per_month = returns.add(1).prod() ** (1 / n_months) - 1
return_per_month

# %%
annualized_return = (1 + return_per_month) ** 12 - 1
annualized_return

# %% [markdown]
# ### Compute ratios

# %%
# Return on risk ratio
annualized_return / annualized_vol

# %%
# Sharpe ratio
risk_free_rate = 0.03
excess_return = annualized_return - risk_free_rate
sharpe_ratio = excess_return / annualized_vol
sharpe_ratio
