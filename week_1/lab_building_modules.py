# %% [markdown]
# # Lab: Building modules
# ## Imports

# %%
import edhec_risk_kit as erk

# %%
returns = erk.get_ffme_returns()
returns.head()

# %%
erk.drawdown(returns.SmallCap).loc[:, "Drawdown"].min()
