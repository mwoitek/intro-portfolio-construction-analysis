# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:percent
#     text_representation:
#       extension: .py
#       format_name: percent
#       format_version: '1.3'
#       jupytext_version: 1.14.4
#   kernelspec:
#     display_name: Python 3 (ipykernel)
#     language: python
#     name: python3
# ---

# %% [markdown]
# # Module 1 Graded Quiz
# ## Imports

# %%
from pathlib import Path
from typing import Tuple

import numpy as np
import pandas as pd

# %% [markdown]
# ## Reading data (Questions 1-12)

# %%
data_path = Path(".").resolve().parent / "data"
file_name = "Portfolios_Formed_on_ME_monthly_EW.csv"
file_path = data_path / file_name

# %%
returns = pd.read_csv(
    file_path,
    index_col=0,
    na_values="-99.99",
)
returns.head()

# %%
# Selecting the columns we'll use
columns = ["Lo 20", "Hi 20"]
returns = returns[columns]
returns.head()

# %% [markdown]
# ## Preparing data (Questions 1-12)

# %%
# Converting from percentages to ratios
returns = returns / 100
returns.head()

# %%
# Fixing the dates (Index of DataFrame)
returns.index = pd.to_datetime(returns.index, format="%Y%m").to_period("M")
returns.head()

# %% [markdown]
# ## Question 1
# What was the annualized return of the Lo 20 portfolio over the entire period?

# %%
def compute_return_per_month(returns_df: pd.DataFrame, portfolio: str) -> np.float_:
    returns_portfolio = returns_df[portfolio]
    num_observations = returns_portfolio.size
    return np.prod(returns_portfolio + 1) ** (1 / num_observations) - 1


# %%
# Computing the return per month (Just a check)
return_per_month = compute_return_per_month(returns, portfolio="Lo 20")
return_per_month

# %%
def compute_annualized_return(returns_df: pd.DataFrame, portfolio: str) -> np.float_:
    return_per_month = compute_return_per_month(returns_df, portfolio)
    return (return_per_month + 1) ** 12 - 1


# %%
# Computing the annualized return
annualized_return = compute_annualized_return(returns, portfolio="Lo 20")
annualized_return

# %%
# Pretty print the result
print(f"Answer: {annualized_return * 100:.2f}")

# %% [markdown]
# ## Question 2
# What was the annualized volatility of the Lo 20 portfolio over the entire
# period?

# %%
def compute_annualized_volatility(returns_df: pd.DataFrame, portfolio: str) -> np.float_:
    volatility = returns_df[portfolio].std()
    return volatility * np.sqrt(12)


# %%
annualized_volatility = compute_annualized_volatility(returns, portfolio="Lo 20")
annualized_volatility

# %%
print(f"Answer: {annualized_volatility * 100:.1f}")

# %% [markdown]
# ## Question 3
# What was the annualized return of the Hi 20 portfolio over the entire period?

# %%
annualized_return = compute_annualized_return(returns, portfolio="Hi 20")
annualized_return

# %%
print(f"Answer: {annualized_return * 100:.2f}")

# %% [markdown]
# ## Question 4
# What was the annualized volatility of the Hi 20 portfolio over the entire
# period?

# %%
annualized_volatility = compute_annualized_volatility(returns, portfolio="Hi 20")
annualized_volatility

# %%
print(f"Answer: {annualized_volatility * 100:.2f}")

# %% [markdown]
# ## Question 5
# What was the annualized return of the Lo 20 portfolio over the period
# 1999-2015 (both inclusive)?

# %%
# Selecting the data that will be used to answer Questions 5-12
returns_99_15 = returns["1999":"2015"]
returns_99_15.head()

# %%
returns_99_15.tail()

# %%
# Computing the annualized return
annualized_return = compute_annualized_return(returns_99_15, portfolio="Lo 20")
annualized_return

# %%
print(f"Answer: {annualized_return * 100:.2f}")

# %% [markdown]
# ## Question 6
# What was the annualized volatility of the Lo 20 portfolio over the period
# 1999-2015 (both inclusive)?

# %%
annualized_volatility = compute_annualized_volatility(returns_99_15, portfolio="Lo 20")
annualized_volatility

# %%
print(f"Answer: {annualized_volatility * 100:.2f}")

# %% [markdown]
# ## Question 7
# What was the annualized return of the Hi 20 portfolio over the period
# 1999-2015 (both inclusive)?

# %%
annualized_return = compute_annualized_return(returns_99_15, portfolio="Hi 20")
annualized_return

# %%
print(f"Answer: {annualized_return * 100:.2f}")

# %% [markdown]
# ## Question 8
# What was the annualized volatility of the Hi 20 portfolio over the period
# 1999-2015 (both inclusive)?

# %%
annualized_volatility = compute_annualized_volatility(returns_99_15, portfolio="Hi 20")
annualized_volatility

# %%
print(f"Answer: {annualized_volatility * 100:.2f}")

# %% [markdown]
# ## Question 9
# What was the max drawdown (expressed as a positive number) experienced over
# the 1999-2015 period in the SmallCap (Lo 20) portfolio?

# %%
def compute_wealth_index(returns_df: pd.DataFrame, portfolio: str) -> pd.Series:
    returns_portfolio = returns_df[portfolio]
    return (returns_portfolio + 1).cumprod()


# %%
def compute_previous_peaks(wealth_index: pd.Series) -> pd.Series:
    return wealth_index.cummax()


# %%
def compute_drawdown(returns_df: pd.DataFrame, portfolio: str) -> pd.Series:
    wealth_index = compute_wealth_index(returns_df, portfolio)
    previous_peaks = compute_previous_peaks(wealth_index)
    return (wealth_index - previous_peaks) / previous_peaks


# %%
def compute_max_drawdown(
    returns_df: pd.DataFrame, portfolio: str
) -> Tuple[np.float_, str]:
    drawdown = compute_drawdown(returns_df, portfolio)
    month_max = drawdown.idxmin()
    return drawdown[month_max], str(month_max)


# %%
# Computing the max drawdown
max_drawdown, month_max = compute_max_drawdown(returns_99_15, portfolio="Lo 20")
max_drawdown

# %%
answer = -max_drawdown * 100
print(f"Answer: {answer:.2f}")

# %% [markdown]
# ## Question 10
# At the end of which month over the period 1999-2015 did that maximum drawdown
# on the SmallCap (Lo 20) portfolio occur?

# %%
print(f"Answer: {month_max}")

# %% [markdown]
# ## Question 11
# What was the max drawdown (expressed as a positive number) experienced over
# the 1999-2015 period in the LargeCap (Hi 20) portfolio?

# %%
max_drawdown, month_max = compute_max_drawdown(returns_99_15, portfolio="Hi 20")
max_drawdown

# %%
answer = -max_drawdown * 100
print(f"Answer: {answer:.2f}")

# %% [markdown]
# ## Question 12
# Over the period 1999-2015, at the end of which month did that maximum drawdown
# of the LargeCap (Hi 20) portfolio occur?

# %%
print(f"Answer: {month_max}")

# %% [markdown]
# ## Reading data (Questions 13-16)

# %%
file_name = "edhec-hedgefundindices.csv"
file_path = data_path / file_name

# %%
hfi_returns = pd.read_csv(file_path, index_col=0)
hfi_returns.head()

# %% [markdown]
# ## Preparing data (Questions 13-16)

# %%
hfi_returns = hfi_returns / 100
hfi_returns.index = pd.to_datetime(hfi_returns.index, format="%d/%m/%Y").to_period("M")
hfi_returns.head()

# %% [markdown]
# ## Question 13
# For the remaining questions, use the EDHEC Hedge Fund Indices data set that we
# used in the lab assignment and load them into Python. Looking at the data
# since 2009 (including all of 2009) through 2018 which Hedge Fund Index has
# exhibited the highest semideviation?

# %%
def compute_semideviation(data: pd.Series) -> np.float_:
    deviation_from_mean = data - data.mean()
    return np.std(data[deviation_from_mean < 0])


# %%
semideviation: pd.Series = hfi_returns["2009":"2018"].apply(compute_semideviation)  # type: ignore
semideviation

# %%
answer = semideviation.idxmax()
print(f"Answer: {answer}")

# %% [markdown]
# ## Question 14
# Looking at the data since 2009 (including all of 2009) which Hedge Fund Index
# has exhibited the lowest semideviation?

# %%
semideviation: pd.Series = hfi_returns["2009":].apply(compute_semideviation)  # type: ignore
semideviation

# %%
answer = semideviation.idxmin()
print(f"Answer: {answer}")

# %% [markdown]
# ## Question 15
# Looking at the data since 2009 (including all of 2009) which Hedge Fund Index
# has been most negatively skewed?

# %%
def compute_standardized_moment(data: pd.Series, degree: int) -> np.float_:
    mu = data.mean()
    sigma = data.std(ddof=0)
    normalized_data = (data - mu) / sigma
    return np.mean(normalized_data**degree)


# %%
def compute_skewness(data: pd.Series) -> np.float_:
    return compute_standardized_moment(data, degree=3)


# %%
skewness: pd.Series = hfi_returns["2009":].apply(compute_skewness)  # type: ignore
skewness

# %%
answer = skewness.idxmin()
print(f"Answer: {answer}")

# %% [markdown]
# ## Question 16
# Looking at the data since 2000 (including all of 2000) through 2018 which
# Hedge Fund Index has exhibited the highest kurtosis?

# %%
def compute_kurtosis(data: pd.Series) -> np.float_:
    return compute_standardized_moment(data, degree=4)


# %%
kurtosis: pd.Series = hfi_returns["2000":"2018"].apply(compute_kurtosis)  # type: ignore
kurtosis

# %%
answer = kurtosis.idxmax()
print(f"Answer: {answer}")
