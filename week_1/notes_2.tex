% Created 2024-09-02 Mon 21:28
% Intended LaTeX compiler: pdflatex
\documentclass[11pt]{article}
\usepackage[american]{babel}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage{longtable}
\usepackage{wrapfig}
\usepackage{rotating}
\usepackage[normalem]{ulem}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{capt-of}
\usepackage{hyperref}
\usepackage{titling}
\usepackage[newfloat]{minted}
\usepackage{float}
\usepackage{enumitem}
\usepackage[a4paper,left=1cm,right=1cm,top=1cm,bottom=1cm]{geometry}
\setlength\parindent{0pt}
\setlength\droptitle{-50pt}
\posttitle{\par\end{center}\vspace{-5em}}
\renewcommand{\labelitemi}{$\rhd$}
\setlist[enumerate]{leftmargin=*}
\setlist[itemize]{leftmargin=*}
\setlist{nosep}
\pagenumbering{gobble}
\date{}
\title{Beyond the Gaussian case: Extreme risk estimates}
\hypersetup{
 pdfauthor={Marcio Woitek},
 pdftitle={Beyond the Gaussian case: Extreme risk estimates},
 pdfkeywords={},
 pdfsubject={},
 pdfcreator={Emacs 29.4 (Org mode 9.8)}, 
 pdflang={English}}
\begin{document}

\maketitle
\section*{Deviations from normality}
\label{sec:org2d93514}

\subsection*{The Gaussian assumption}
\label{sec:org22f4c6e}

\begin{itemize}
\item \textbf{Standard simplifying assumption}: Asset returns are normally distributed.
\item When this kind of assumption is correct, the probability associated with
extreme values is very low.
\end{itemize}
\subsection*{Large chances are more frequent in reality}
\label{sec:org371f5d0}

\begin{itemize}
\item When we analyze actual stock returns, it isn't uncommon to observe
extreme values.
\item In the lecture, this fact is illustrated using the daily returns for Nike.
\item This is our first argument against the Gaussian assumption.
\end{itemize}
\subsection*{Higher moments}
\label{sec:org717adcb}

\begin{itemize}
\item \textbf{Skewness}: Measure of the asymmetry of a distribution.
\item The skewness of the return distribution can be computed as follows:
\begin{equation}
\label{eq:skewness}
S(R)=\frac{\mathrm{E}\left[(R-\mathrm{E}[R])^3\right]}{[\mathrm{Var}(R)]^{3/2}},
\end{equation}
where \(R\) is the return, \(\mathrm{E}\) denotes the expected value,
and \(\mathrm{Var}\) corresponds to the variance.
\item Since the Gaussian distribution is symmetric, the corresponding skewness
equals 0.
\item \textbf{Kurtosis}: Measure of the thickness of the tails of a distribution.
\item This measure can be computed with the aid of this formula:
\begin{equation}
\label{eq:kurtosis}
K(R)=\frac{\mathrm{E}\left[(R-\mathrm{E}[R])^4\right]}{[\mathrm{Var}(R)]^2}.
\end{equation}
\item It's possible to show that, for a normal distribution, the kurtosis is 3.
\item \textbf{Excess kurtosis}: Kurtosis minus 3.
\end{itemize}
\subsection*{Evidence of non-normality}
\label{sec:orgccdcd00}

\begin{itemize}
\item In the lecture, some hedge funds are considered.
\item A table containing values of skewness and excess kurtosis is presented.
\item All of those values are significantly different from zero, which
indicates deviation from normality.
\item \textbf{Jarque-Bera test}: Statistical test to determine whether sample data has
the skewness and kurtosis matching a Gaussian distribution.
\item Corresponding test statistic:
\begin{equation}
\label{eq:jarque-bera-test-statistic}
\mathrm{JB}=\frac{n}{6}\left[S^2+\frac{(K-3)^2}{4}\right],
\end{equation}
where \(n\) is the sample size, \(S\) is the sample skewness, and
\(K\) is the sample kurtosis.
\item When the sample data comes from a normal distribution, the JB statistic
has a chi-squared distribution with 2 degrees of freedom.
\item The table from the lecture also shows results of the Jarque-Bera test for
the hedge fund returns.
\item In all cases, the null hypothesis (normality) is rejected.
\item Stock returns are also considered.
\item An example is shown where once again there's evidence of non-Gaussian behavior.
\end{itemize}
\section*{Downside risk measures}
\label{sec:org4d64708}

\subsection*{Volatility versus semi-deviation}
\label{sec:orgb2037f6}

\begin{itemize}
\item \textbf{Semi-deviation}: Volatility of the sub-sample of below-average or
below-zero returns.
\item Investors are not bothered by volatility on the upside.
\item Semi-deviation is a measure of volatility on the downside.
\item When we consider below-average returns, the corresponding formula is
\begin{equation}
\label{eq:semi-deviation}
\sigma_{\mathrm{semi}}=\sqrt{\frac{1}{N}\sum_{R_i\leq\bar{R}}\left(R_i-\bar{R}\right)^2},
\end{equation}
where \(R_i\) is the \(i\)-th return, \(\bar{R}\) is the average
return, and \(N\) is the number of observations that fall below the average.
\end{itemize}
\subsection*{Value at Risk (VaR)}
\label{sec:org854d105}

\begin{itemize}
\item \textbf{VaR}: Maximum expected loss over a given time period.
\item VaR is defined at a specified confidence level (e.g., 99\%).
\item Moreover, VaR is computed over a specified period (e.g., 1 month).
\item To calculate the VaR, first we exclude a certain percentage (e.g., 1\%) of
the worst returns. Then we identify the worst return amongst the ones we
kept. This return corresponds to the VaR.
\item In practice, if we want to determine the VaR for a confidence level
\(\alpha\), then we need to find the \((100-\alpha)\)-th percentile
of the returns.
\item This percentile is likely to be a negative number, i.e., a loss.
\item However, we ignore the sign, and report the VaR as a positive number.
\end{itemize}
\subsection*{Conditional VaR (CVaR)}
\label{sec:orgac3ff92}

\begin{itemize}
\item \textbf{CVaR}: Expected loss beyond VaR.
\item In this case, the word "beyond" is a little misleading.
\item Basically, we're talking about the returns that are worse than the VaR.
\item The CVaR is the average of these returns.
\item Equivalently: the conditional VaR is the average of the worst returns as
defined by the VaR.
\item This mean is usually a negative number, but we ignore its sign.
\item The adjective "conditional" is used, since the CVaR can be written as a
conditional expectation value:
\begin{equation}
\label{eq:cvar}
\mathrm{CVaR}=-\mathrm{E}[R|R\leq-\mathrm{VaR}],
\end{equation}
where \(R\) denotes the return.
\end{itemize}
\end{document}
