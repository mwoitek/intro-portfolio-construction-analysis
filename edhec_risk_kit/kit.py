from pathlib import Path

import numpy as np
import pandas as pd
from scipy import stats


def drawdown(return_series: pd.Series) -> pd.DataFrame:
    """
    Takes a time series of asset returns.
    Computes and returns a DataFrame that contains:
    - the wealth index,
    - the previous peaks,
    - percent drawdowns.
    """
    wealth_index = 1000 * (1 + return_series).cumprod()
    previous_peaks = wealth_index.cummax()
    drawdowns = (wealth_index - previous_peaks) / previous_peaks
    return pd.DataFrame(
        data={
            "Wealth": wealth_index,
            "Peaks": previous_peaks,
            "Drawdown": drawdowns,
        }
    )


def get_ffme_returns() -> pd.DataFrame:
    """
    Load the Fama-French dataset for the returns of the top and bottom
    deciles by market cap.
    """
    rets = pd.read_csv(
        Path(__file__).resolve().parents[1] / "data" / "Portfolios_Formed_on_ME_monthly_EW.csv",
        header=0,
        index_col=0,
        na_values=["-99.99"],
    )
    rets.index = pd.to_datetime(rets.index, format="%Y%m").to_period("M")
    rets = rets[["Lo 10", "Hi 10"]]
    rets.columns = ["SmallCap", "LargeCap"]
    rets = rets / 100
    return rets


def get_hfi_returns() -> pd.DataFrame:
    """
    Load and format the EDHEC Hedge Fund Index Returns.
    """
    hfi = pd.read_csv(
        Path(__file__).resolve().parents[1] / "data" / "edhec-hedgefundindices.csv",
        header=0,
        index_col=0,
    )
    hfi.index = pd.to_datetime(hfi.index, format="%d/%m/%Y").to_period("M")
    hfi = hfi / 100
    return hfi


def get_ind_returns() -> pd.DataFrame:
    """
    Load and format the Ken French 30 Industry Portfolios Value Weighted
    Monthly Returns.
    """
    ind = pd.read_csv(
        Path(__file__).resolve().parents[1] / "data" / "ind30_m_vw_rets.csv",
        header=0,
        index_col=0,
    )
    ind.columns = ind.columns.str.strip()
    ind.index = pd.to_datetime(ind.index, format="%Y%m").to_period("M")
    ind /= 100
    return ind


def skewness(r: pd.Series | pd.DataFrame) -> float | pd.Series:
    """
    Computes the skewness of the supplied Series or DataFrame.
    Returns a float or a Series.
    """
    demeaned_r = r - r.mean()
    exp = demeaned_r.pow(3).mean()
    sigma_r = r.std(ddof=0)
    return exp / sigma_r**3


def kurtosis(r: pd.Series | pd.DataFrame) -> float | pd.Series:
    """
    Computes the kurtosis of the supplied Series or DataFrame.
    Returns a float or a Series.
    """
    demeaned_r = r - r.mean()
    exp = demeaned_r.pow(4).mean()
    sigma_r = r.std(ddof=0)
    return exp / sigma_r**4


def is_normal(r: pd.Series, level: float = 0.01) -> bool:
    """
    Applies the Jarque-Bera test to determine if a Series is normal or not.
    Test is applied at the 1% level by default.
    Returns False if the hypothesis of normality is rejected, True otherwise.
    """
    _, p_value = stats.jarque_bera(r)
    p_value = float(p_value)  # pyright: ignore
    return p_value > level


def semideviation(r: pd.Series | pd.DataFrame) -> float | pd.Series:
    """
    Returns the negative semi-deviation of r.
    r must be a Series or DataFrame.
    """
    return r[r < 0].std(ddof=0)  # pyright: ignore


def var_historic(r: pd.Series | pd.DataFrame, level: int | float = 5) -> float | pd.Series:
    """
    Returns the Historic Value at Risk at a specified level.
    In other words, returns the number such that `level` percent of the
    returns fall below that number.
    """
    # NOTE: Function from the lecture is unnecessarily convoluted.
    if isinstance(r, pd.Series):
        return -float(np.percentile(r, level))
    return -r.aggregate(lambda c: np.percentile(c, level))


def var_gaussian(
    r: pd.Series | pd.DataFrame,
    level: int | float = 5,
    modified: bool = False,
) -> float | pd.Series:
    """
    Returns the Parametric Gaussian VaR of a Series or DataFrame.
    If `modified` is True, then the modified VaR is returned, using the
    Cornish-Fisher modification.
    """
    z = stats.norm.ppf(level / 100)
    if modified:
        s = skewness(r)
        k = kurtosis(r)
        z += (z**2 - 1) * s / 6 + (z**3 - 3 * z) * (k - 3) / 24 - (2 * z**3 - 5 * z) * s**2 / 36
    var = -(r.mean() + z * r.std(ddof=0))
    return var if isinstance(var, pd.Series) else float(var)


def cvar_historic(r: pd.Series | pd.DataFrame, level: int | float = 5) -> float | pd.Series:
    """
    Computes the Conditional VaR of a Series or DataFrame.
    """
    if isinstance(r, pd.Series):
        is_beyond = r <= -var_historic(r, level=level)
        return -r[is_beyond].mean()
    return r.aggregate(cvar_historic, level=level)


def annualize_rets(r: pd.Series | pd.DataFrame, periods_per_year: int) -> float | pd.Series:
    """
    Annualize a set of returns.
    """
    # NOTE: I changed the code from the lecture to satisfy Pyright.
    if isinstance(r, pd.Series):
        compounded_growth = float(np.prod(1 + r))
        n_periods = len(r)
        return compounded_growth ** (periods_per_year / n_periods) - 1
    return r.aggregate(annualize_rets, periods_per_year=periods_per_year)


def annualize_vol(r: pd.Series | pd.DataFrame, periods_per_year: int) -> float | pd.Series:
    """
    Annualize the volatility of a set of returns.
    """
    return r.std() * np.sqrt(periods_per_year)


def sharpe_ratio(
    r: pd.Series | pd.DataFrame,
    risk_free_rate: float,
    periods_per_year: int,
) -> float | pd.Series:
    """
    Computes the annualized Sharpe ratio of a set of returns.
    """
    # Convert the annual risk-free rate to per period
    rf_per_period: float = (1 + risk_free_rate) ** (1 / periods_per_year) - 1
    excess_ret = r - rf_per_period
    ann_ex_ret = annualize_rets(excess_ret, periods_per_year)
    ann_vol = annualize_vol(r, periods_per_year)
    return ann_ex_ret / ann_vol
